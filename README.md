# Kiwi task related documentation:

- setup a cluster of your preference on GKE and put its name into memory

- `./deploy_to_GKE.sh` will need a bit editing: replace the cluster name with the one you created.

- run `./deploy_to_GKE.sh`
  - There is instruction on pointing your browser to the newly running app inside the file

- Application should respond with JSON on some port.
  - `/somejson` does so

 - - - -
Follows some description of the application...

# Complex Orders to Binance - app sketch, currently

pre-prototype, demonstrating pre-basic functions of *ccxt* over Flask, so we can show it publicly, from GKE cluster currently.

This is reused sketch of older development code modified for Kiwi.com Summer Camp intro task.


