 # New deployment of flaskapp via k8s CLI:
 kubectl get pods # optional, you can check out there is no pod of name "flaska".
 kubectl run flaska --image docker.io/uhuge/flaskapp:gke --port 80
 kubectl expose deployment flaska --type LoadBalancer   --port 80 --target-port 80
 #Grab public IP the service points to:
 #$pubIP = kubectl get service flaska -o custom-columns=custom-columns=CONTAINER:.spec.containers[0].name #
 kubectl get service flaska -o custom-columns=custom-columns=CONTAINER:.spec.containers[0].name
 #xdg-open http://104.198.70.217
 ## Last step is making more replicas of the app for better resilience/availability: 
 kubectl scale --replicas=2 deployment flaska #manual scaling then export it via `get -o yaml` +)
 #kubectl get deployments.apps flaska -o yaml|less # one way to see replication is set up
